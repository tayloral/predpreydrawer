/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preditorpreyplotter;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adam
 */
public class DataReader implements Runnable
{

      private boolean useUDP = false;
      private int listenPort;
      private String filename;
      private boolean isRunning = true;
      private Queue<String> messages;

      public DataReader()
      {
	    messages = new LinkedList<>();
      }

      @Override
      public void run()
      {
	    if (useUDP)
	    {
		  DatagramSocket wSocket = null;
		  DatagramPacket wPacket = null;
		  byte[] wBuffer = null;

		  try
		  {
			wSocket = new DatagramSocket(getListenPort());
			wBuffer = new byte[4096];
			wPacket = new DatagramPacket(wBuffer, wBuffer.length);
			System.out.println(wSocket.getReceiveBufferSize());
			wSocket.setReceiveBufferSize(wSocket.getReceiveBufferSize() * 3);
		  }
		  catch (SocketException e)
		  {
			System.out.println("Could not open the socket: \n" + e.getMessage());
			System.exit(1);
		  }

		  while (isIsRunning())
		  {
			try
			{
			      wSocket.receive(wPacket);
			      handlePacket(wPacket, wBuffer);
			}
			catch (Exception e)
			{
			      System.out.println(e.getMessage());
			}
		  }
		  System.err.println("reading thread shutdown by bool ");
	    }
	    else
	    {
		  try ( //use files
			  FileInputStream inputStream = new FileInputStream(getFilename()))
		  {
			Scanner s = new Scanner(inputStream).useDelimiter("\\A");
			String everything = s.hasNext() ? s.next() : "";
			String[] split = everything.split("<timeStep>");
			for (String split1 : split)
			{
			      if (split1.length() > 2)
			      {
				    messages.offer(split1);
			      }
			}
		  }
		  catch (IOException ex)
		  {
			Logger.getLogger(DataReader.class.getName()).log(Level.SEVERE, null, ex);
		  }
	    }

      }

      private void handlePacket(DatagramPacket wPacket, byte[] wBuffer)
      {
	    //System.err.println(new String(wPacket.getData()));
	    if (!messages.offer(new String(wBuffer)))
	    {
		  System.err.println("Couldn't add to the queue the message from " + wPacket.getSocketAddress().toString());
	    }
	    //System.out.println("recieved - Messages in queue= " + messages.size());
      }

      public synchronized String getNextMessage()
      {
//	    System.out.println("Messages in queue= " + (messages.size() - 1));
	    return messages.poll();
      }

      /**
       * @return the listenPort
       */
      public int getListenPort()
      {
	    return listenPort;
      }

      /**
       * @param listenPort the listenPort to set
       */
      public void setListenPort(int listenPort)
      {
	    useUDP = true;
	    this.listenPort = listenPort;
      }

      /**
       * @return the isRunning
       */
      public boolean isIsRunning()
      {
	    return isRunning;
      }

      /**
       * @param isRunning the isRunning to set
       */
      public void setIsRunning(boolean isRunning)
      {
	    this.isRunning = isRunning;
      }

      /**
       * @return the filename
       */
      public String getFilename()
      {
	    return filename;
      }

      /**
       * @param filename the filename to set
       */
      public void setFilename(String filename)
      {
	    this.useUDP = false;
	    this.filename = filename;
      }

}
