/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preditorpreyplotter;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Adam
 */
public class DataDrawer
{

      private final JFrame controls;
      private String controlsTitle = "Speed Controls - Current Frame: ";
      private boolean playing = false;
      private boolean allowingAutoPause = true;
      private int frame = 0;
      private double frameRate = 8.5;//in fps
      private int numberOfFrames = 0;
      private final int historyLength = 3;
      private long nextFrameTime;

      private final WorldComponent worldComponent;
      private final JFrame world;
      JTextField frameRateIn;
      JButton autoPauseButton;
      JLabel currentMaxFrames;
      JSlider selectFrame;
      private boolean processedReset = true;

      public DataDrawer()
      {
	    Dimension controlSize = new Dimension(450, 175);
	    //make the controls
	    controls = new JFrame("Speed Controls");
	    controls.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    controls.setMinimumSize(controlSize);
	    controls.setPreferredSize(controlSize);
	    controls.getContentPane().setLayout(new GridBagLayout());

	    GridBagConstraints c = new GridBagConstraints();

	    JButton playButton = new JButton("PLAY");
	    playButton.addActionListener(new ActionListener()
	    {

		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			playing = true;
		  }
	    });

	    c.anchor = GridBagConstraints.FIRST_LINE_START;
	    c.insets = new Insets(0, 0, 25, 0);
	    controls.getContentPane().add(playButton, c);

	    JButton pauseButton = new JButton("PAUSE");
	    pauseButton.addActionListener(new ActionListener()
	    {

		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			playing = false;
		  }
	    });
	    c.anchor = GridBagConstraints.PAGE_START;
	    c.insets = new Insets(0, 0, 25, 0);
	    controls.getContentPane().add(pauseButton, c);

	    JButton resetButton = new JButton("RESET");
	    resetButton.addActionListener(new ActionListener()
	    {
		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			playing = false;
			frame = 0;
			processedReset = false;
			getFrame(frame);
		  }
	    });
	    c.anchor = GridBagConstraints.FIRST_LINE_END;
	    c.insets = new Insets(0, 0, 25, 0);
	    controls.getContentPane().add(resetButton, c);

	    JLabel startFrameRate = new JLabel("" + 0);
	    c.gridy = 1;
	    //c.gridwidth = GridBagConstraints.RELATIVE;
	    c.insets = new Insets(0, 0, 25, 0);
	    c.anchor = GridBagConstraints.LINE_START;
	    controls.getContentPane().add(startFrameRate, c);

	    //Create the slider.
	    selectFrame = new JSlider(JSlider.HORIZONTAL, 0, numberOfFrames, numberOfFrames / 100);
	    selectFrame.addChangeListener(new ChangeListener()
	    {
		  @Override
		  public void stateChanged(ChangeEvent e)
		  {
			frame = selectFrame.getValue();
			getFrame(selectFrame.getValue());
		  }
	    });
	    c.gridy = 1;
	    //c.gridwidth = GridBagConstraints.REMAINDER;
	    c.insets = new Insets(0, 0, 25, 0);
	    c.anchor = GridBagConstraints.CENTER;
	    controls.getContentPane().add(selectFrame, c);

	    currentMaxFrames = new JLabel("" + numberOfFrames);
	    c.gridy = 1;
	    //c.gridwidth = GridBagConstraints.REMAINDER;
	    c.insets = new Insets(0, 0, 25, 0);
	    c.anchor = GridBagConstraints.LINE_END;
	    controls.getContentPane().add(currentMaxFrames, c);

	    c = new GridBagConstraints();

	    frameRateIn = new JTextField("" + frameRate);
	    c.gridy = 2;
	    c.insets = new Insets(0, 0, 0, 0);
	    // c.weightx = 1;
	    c.anchor = GridBagConstraints.LAST_LINE_START;
	    controls.getContentPane().add(frameRateIn, c);

	    JButton rateButton = new JButton("SET RATE");
	    rateButton.addActionListener(new ActionListener()
	    {

		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			frameRate = Double.parseDouble(frameRateIn.getText());
			frameRateIn.setText("" + frameRate);
			controls.repaint();
		  }
	    });
	    //c.weightx = 1;
	    c.anchor = GridBagConstraints.PAGE_END;
	    c.gridy = 2;
	    c.insets = new Insets(0, 0, 0, 0);
	    controls.getContentPane().add(rateButton, c);

	    autoPauseButton = new JButton("AUTOPAUSE ON");
	    autoPauseButton.addActionListener(new ActionListener()
	    {
		  @Override
		  public void actionPerformed(ActionEvent e)
		  {
			allowingAutoPause = !allowingAutoPause;
			if (allowingAutoPause)
			{
			      autoPauseButton.setText("AUTOPAUSE ON");
			}
			else
			{
			      autoPauseButton.setText("AUTOPAUSE OFF");
			}
		  }
	    });
	    //c.weightx = 1;
	    c.anchor = GridBagConstraints.LAST_LINE_END;
	    c.gridy = 2;
	    c.insets = new Insets(0, 0, 0, 0);
	    controls.getContentPane().add(autoPauseButton, c);

	    //now the main window
	    Dimension mainSize = new Dimension(1000, 1000);
	    world = new JFrame("World");
	    world.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    world.setMinimumSize(mainSize);
	    world.setPreferredSize(mainSize);
	    world.setLocation(controlSize.width + 20, 0);

	    worldComponent = new WorldComponent();
	    world.getContentPane().add(worldComponent);

	    //all done make it
	    controls.pack();
	    controls.setVisible(true);
	    world.setVisible(true);

	    nextFrameTime = System.currentTimeMillis() + (Long) Math.round(1000 / frameRate);//curent time plus millis in second/ frmaes per second
      }

      public void manageFrameProgress()
      {
	    if (System.currentTimeMillis() > nextFrameTime)
	    {//if we are due a new frame
		  if (playing)
		  {//move to the next one
			frame++;
			if (frame > PreditorPreyPlotter.parser.numberOfMessages())
			{//prevent playing past end
			      frame = PreditorPreyPlotter.parser.numberOfMessages();
			      playing = false;
			}

			getFrame(frame);//push it to renderer
		  }

		  if (worldComponent.allSet() == false)
		  {
			//System.out.println("Here");
			getFrame(frame);//push it to renderer

		  }

		  nextFrameTime = System.currentTimeMillis() + (Long) Math.round(1000 / frameRate);//curent time plus millis in second/ frmaes per second
	    }
	    //System.out.println("Current Frame is " + frame + " at " + System.currentTimeMillis());
      }

      /**
       * *
       * get the indicated frame and the history and push them to the display
       *
       * @param time
       */
      public void getFrame(int time)
      {
	    //do some ui stuf to make current fram clear
	    controls.setTitle(controlsTitle + " " + time);
	    selectFrame.setValue(time);
	    numberOfFrames = PreditorPreyPlotter.parser.numberOfMessages();
	    selectFrame.setMaximum(numberOfFrames);
	    currentMaxFrames.setText("" + numberOfFrames);
	    /*System.out.println("GetFrame");*/
	    String currentFrame = PreditorPreyPlotter.parser.getMessage(time);
	    if (currentFrame != null)
	    {
		  Point autoPause = DataParser.hasAutoPause(currentFrame);
		  if (allowingAutoPause && autoPause != null)
		  {//if is an auto pause
			playing = false;
			worldComponent.setAutoPause(autoPause);
		  }

		  if (processedReset == false || worldComponent.allSet() == false)
		  {//not set so set
			worldComponent.setNumberOfPreditors(DataParser.parseNumberOfPreditors(currentFrame));
			worldComponent.setNumberOfPreys(DataParser.parseNumberOfPreys(currentFrame));
			worldComponent.setWorldWidth(DataParser.parseWorldX(currentFrame));
			worldComponent.setWorldHigth(DataParser.parseWorldY(currentFrame));
			worldComponent.setWorld(DataParser.parseWorld(currentFrame));
			worldComponent.setHistoryLength(historyLength);
			worldComponent.clearTraces();
			for (int a = 0; a < historyLength; a++)
			{
			      String frameToParse = PreditorPreyPlotter.parser.getMessage(time - a);
			      if (frameToParse != null)
			      {//check it's there
				    if (worldComponent.getNumberOfPreditors() > 0)
				    {
					  List<String> parsePreditors = DataParser.parsePreditors(frameToParse);
					  for (int b = 0; b < parsePreditors.size(); b++)
					  {//add each pred
						// System.out.println("b= " + b + " " + parsePreditors[b]);
						worldComponent.addPredatorTraceStep(DataParser.parsePoint(parsePreditors.get(b)), b, a);
					  }
				    }
				    if (worldComponent.getNumberOfPrey() > 0)
				    {
					  List<String> parsePrey = DataParser.parsePrey(frameToParse);
					  for (int b = 0; b < parsePrey.size(); b++)
					  {//add each pred
						worldComponent.addPreyTraceStep(DataParser.parsePoint(parsePrey.get(b)), b, a);
					  }
				    }
			      }

			}
			processedReset = true;
		  }
		  else
		  {//all is set read history
			worldComponent.clearTraces();
			for (int a = 0; a < historyLength; a++)
			{
			      String frameToParse = PreditorPreyPlotter.parser.getMessage(time - a);
			      if (frameToParse != null)
			      {//check it's there
				    if (worldComponent.getNumberOfPreditors() > 0)
				    {
					  List<String> parsePreditors = DataParser.parsePreditors(frameToParse);
					  for (int b = 0; b < parsePreditors.size(); b++)
					  {//add each pred
						// System.out.println("b= " + b + " " + parsePreditors[b]);
						worldComponent.addPredatorTraceStep(DataParser.parsePoint(parsePreditors.get(b)), b, a);
					  }
				    }
				    if (worldComponent.getNumberOfPrey() > 0)
				    {
					  List<String> parsePrey = DataParser.parsePrey(frameToParse);
					  for (int b = 0; b < parsePrey.size(); b++)
					  {//add each pred
						worldComponent.addPreyTraceStep(DataParser.parsePoint(parsePrey.get(b)), b, a);
					  }
				    }
			      }
			}
		  }
	    }
	    else
	    {
		  //System.out.println("No frame index= " + time);
	    }
	    world.setPreferredSize(worldComponent.getSize());
	    world.setMinimumSize(worldComponent.getSize());
	    world.setMaximumSize(worldComponent.getSize());
	    this.worldComponent.repaint();
      }
}
