/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preditorpreyplotter;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adam
 */
public class DataParser
{
      
      private final List<String> history;
      
      static final String noPredTag = "<noPred>";
      static final String noPreyTag = "<noPrey>";
      static final String predTag = "<Pred>";
      static final String preyTag = "<Prey>";
      static final String worldXTag = "<worX>";
      static final String worldYTag = "<worY>";
      static final String worldTag = "<Worl>";
      static final String posXTag = "<posX>";
      static final String posYTag = "<posY>";
      private final static String pauseTag = "<pause>";
      
      static int parseNumberOfPreditors(String currentFrame)
      {
	    int index = currentFrame.indexOf(noPredTag);
	    index += noPredTag.length() + 1;//for tag and opening <
	    String value = currentFrame.substring(index, currentFrame.indexOf(">", index));
	    return Integer.parseInt(value);
	    
      }

      /**
       * *
       * see if there is a pause to do, if so remove the tag
       *
       * @param currentFrame
       * @return
       */
      static Point hasAutoPause(String currentFrame)
      {
	    if (currentFrame.length() > 0 && currentFrame.contains(pauseTag))
	    {
		  Point parsePoint = DataParser.parsePoint(currentFrame.substring(currentFrame.indexOf(pauseTag)));
		  return parsePoint;
	    }
	    else
	    {
		  return null;
	    }
      }
      
      static int parseNumberOfPreys(String currentFrame)
      {
	    int index = currentFrame.indexOf(noPreyTag);
	    index += noPreyTag.length() + 1;//for tag and opening <
	    String value = currentFrame.substring(index, currentFrame.indexOf(">", index));
	    return Integer.parseInt(value);
      }
      
      static int parseWorldX(String currentFrame)
      {
	    int index = currentFrame.indexOf(worldXTag);
	    index += worldXTag.length() + 1;//for tag and opening <
	    String value = currentFrame.substring(index, currentFrame.indexOf(">", index));
	    return Integer.parseInt(value);
      }
      
      static int parseWorldY(String currentFrame)
      {
	    int index = currentFrame.indexOf(worldYTag);
	    index += worldYTag.length() + 1;//for tag and opening <
	    String value = currentFrame.substring(index, currentFrame.indexOf(">", index));
	    return Integer.parseInt(value);
      }
      
      static String parseWorld(String currentFrame)
      {
	    int index = currentFrame.indexOf(worldTag);
	    index += worldTag.length() + 1;//for tag and opening <	     
	    return currentFrame.substring(index, currentFrame.indexOf(">", index));
      }
      
      static List<String> parsePrey(String currentFrame)
      {
	    int index = currentFrame.indexOf(preyTag);
	    List<String> outList = new ArrayList<>();
	    //index = currentFrame.indexOf(">", index) + 1;//end on num prey
	    String substring = currentFrame.substring(index, currentFrame.indexOf(noPredTag, index));
	    String[] potentialOutput = substring.split(preyTag);
	    for (String s : potentialOutput)
	    {
		  if (s != null && s.length() > 0)
		  {
			outList.add(s);
		  }
	    }
	    return outList;
      }
      
      static Point parsePoint(String frame)
      {
	    Point output = new Point();
	    int index = frame.indexOf(posXTag);
	    index += posXTag.length() + 1;
	    output.x = Integer.parseInt(frame.substring(index, frame.indexOf(">", index)));
	    
	    index = frame.indexOf(posYTag);
	    index += posYTag.length() + 1;
	    output.y = Integer.parseInt(frame.substring(index, frame.indexOf(">", index)));
	    return output;
      }
      
      static List<String> parsePreditors(String currentFrame)
      {
	    int index = currentFrame.indexOf(predTag);
	    List<String> outList = new ArrayList<>();
	    //index = currentFrame.indexOf(">", index) + 1;//end on num prey
	    String substring = currentFrame.substring(index, currentFrame.length());
	    String[] potentialOutput = substring.split(predTag);
	    for (String s : potentialOutput)
	    {
		  if (s != null && s.length() > 0)
		  {
			outList.add(s);
		  }
	    }
	    return outList;
      }
      
      public DataParser()
      {
	    this.history = new ArrayList<>();
      }
      
      public void addMessage(String input)
      {
	    if (input == null)
	    {
		  //nothing to add
	    }
	    else if (history.add(input) == false)
	    {
		  System.out.println("Failed to add message to the history");
	    }
	    else
	    {
		  //System.out.println("Added message size= " + input.length());
		  //System.out.println(input);
	    }
      }
      
      public String getMessage(int time)
      {
	    if (time < history.size() && time >= 0)
	    {
		  return history.get(time);
	    }
	    else
	    {
		  return null;
	    }
      }
      
      int numberOfMessages()
      {
	    return this.history.size();
      }
}
