/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preditorpreyplotter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Adam
 */
public class WorldComponent extends JPanel
{

      private final Color[] preditorColourMap =
      {
	    new Color(255, 204, 204), new Color(255, 153, 153), new Color(255, 102, 102), new Color(255, 51, 51), new Color(255, 0, 0), new Color(204, 0, 0), new Color(153, 0, 0), new Color(102, 0, 0)
      };
      private final Color[] preyColourMap =
      {
	    new Color(204, 255, 204), new Color(153, 255, 153), new Color(102, 255, 102), new Color(51, 255, 51), new Color(0, 255, 0), new Color(0, 204, 0), new Color(0, 153, 0), new Color(0, 102, 0)
      };
      private int worldWidth = -1;
      private int historyLength;
      private int worldHigth = -1;
      private int numberOfPreditors = -1;
      private int numberOfPrey = -1;
      private List<List<Point>> predatorTraces = null;
      private List<List<Point>> preyTraces = null;
      private Point autoPause = null;
      private char[][] world = null;

      public WorldComponent()
      {
	    predatorTraces = new ArrayList<>();
	    preyTraces = new ArrayList<>();
      }

      /**
       * @param worldWidth the worldWidth to set
       */
      public void setWorldWidth(int worldWidth)
      {
	    this.worldWidth = worldWidth;
	    if (worldHigth != -1)
	    {//both set 
		  world = new char[worldWidth][worldHigth];
	    }
      }

      /**
       * @param worldHigth the worldHigth to set
       */
      public void setWorldHigth(int worldHigth)
      {
	    this.worldHigth = worldHigth;
	    if (worldWidth != -1)
	    {//both set 
		  world = new char[worldWidth][worldHigth];
	    }
      }

      /**
       * @param numberOfPreditors the numberOfPreditors to set
       */
      public void setNumberOfPreditors(int numberOfPreditors)
      {
	    this.numberOfPreditors = numberOfPreditors;
      }

      /**
       * @param numberOfPrey the numberOfPrey to set
       */
      public void setNumberOfPreys(int numberOfPrey)
      {
	    this.numberOfPrey = numberOfPrey;
      }

      @Override
      public void paint(Graphics g)
      {
	    boolean drawIndexs = true;
	    super.paint(g);
	    //clear any stuff and pain it black
	    g.clearRect(0, 0, getWidth(), getHeight());
	    if (allSet() == false)
	    {
		  String message = "P Waiting for world to be sent";
		  g.drawString(message, this.getWidth() / 2 - (g.getFontMetrics().stringWidth(message) / 2), this.getHeight() / 2);
	    }

	    //draw the world
	    for (int a = 0; a < worldWidth; a++)
	    {
		  //for (int index = worldHigth - 1; index >= 0; index--)
		  for (int index = 0; index < worldHigth; index++)
		  {
			int b = worldHigth - index - 1;
			String message;
			if (autoPause != null && autoPause.x == a && autoPause.y == b)
			{//if we need to draw an auto pause
			      g.setColor(Color.MAGENTA);
			      g.fillRect(a * 50, index * 50, 50, 50);
			      if (drawIndexs)
			      {
				    message = a + ", " + b;
				    g.setColor(Color.WHITE);
				    g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
			      }
			      autoPause = null;
			}
			else
			{
			      switch (world[a][b])
			      {
				    case '+':
					  g.setColor(Color.BLACK);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case '|':
					  g.setColor(Color.BLACK);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case '-':
					  g.setColor(Color.BLACK);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case 'E':
					  g.setColor(Color.cyan);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.black);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case 'I':
					  g.setColor(Color.gray);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case 'P':
					  g.setColor(Color.RED);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case 'F':
					  g.setColor(Color.GREEN);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    case '?':
					  g.setColor(Color.pink);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
				    default:
					  char toTest = world[a][b];
					  if (Character.isUpperCase(toTest))
					  {//if is a pred
						g.setColor(Color.LIGHT_GRAY);
						g.fillRect(a * 50, index * 50, 50, 50);
						if (drawIndexs)
						{
						      message = a + ", " + b;
						      g.setColor(Color.black);
						      g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
						}
						break;
					  }
					  if (Character.isLowerCase(toTest))
					  {//if is a prey
						g.setColor(Color.LIGHT_GRAY);
						g.fillRect(a * 50, index * 50, 50, 50);
						if (drawIndexs)
						{
						      message = a + ", " + b;
						      g.setColor(Color.black);
						      g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
						}
						break;
					  }
					  g.setColor(Color.MAGENTA);
					  g.fillRect(a * 50, index * 50, 50, 50);
					  if (drawIndexs)
					  {
						message = a + ", " + b;
						g.setColor(Color.WHITE);
						g.drawString(message, a * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), index * 50 + 25);
					  }
					  break;
			      }
			}

		  }
	    }
	    //now draw preds and prey
	    for (int a = 0; a < predatorTraces.size(); a++)
	    {
		  List<Point> trace = predatorTraces.get(a);
		  for (int b = 0; b < trace.size(); b++)
		  {
			if (trace.get(b).x != -1 && trace.get(b).y != -1)
			{
			      g.setColor(preditorColourMap[MapToRange(a, 0, predatorTraces.size(), 0, preditorColourMap.length)]);
			      g.fillOval(trace.get(b).x * 50 + (5 * b), (worldHigth - trace.get(b).y - 1) * 50 + (5 * b), 50 - (10 * b), 50 - (10 * b));
			      if (drawIndexs && b == 0)
			      {
				    String message = "Pred" + a;
				    g.setColor(Color.black);
				    g.drawString(message, trace.get(b).x * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), (worldHigth - trace.get(b).y - 1) * 50 + 25);
			      }
			}
		  }
	    }
	    for (int a = 0; a < preyTraces.size(); a++)
	    {
		  List<Point> trace = preyTraces.get(a);
		  for (int b = 0; b < trace.size(); b++)
		  {
			if (trace.get(b).x != -1 && trace.get(b).y != -1)
			{
			      g.setColor(preyColourMap[MapToRange(a, 0, preyTraces.size(), 0, preyColourMap.length)]);
			      g.fillOval(trace.get(b).x * 50 + (5 * b), (worldHigth - trace.get(b).y - 1) * 50 + (5 * b), 50 - (10 * b), 50 - (10 * b));
			      if (drawIndexs && b == 0)
			      {
				    String message = "Prey" + a;
				    g.setColor(Color.black);
				    g.drawString(message, trace.get(b).x * 50 + 25 - (g.getFontMetrics().stringWidth(message) / 2), (worldHigth - trace.get(b).y - 1) * 50 + 25);
			      }
			}
		  }
	    }
      }

      public boolean allSet()
      {
	    if (worldHigth == -1 || worldWidth == -1 || getNumberOfPreditors() == -1 || getNumberOfPrey() == -1 || preyTraces == null || predatorTraces == null || world == null)
	    {
		  return false;
	    }
	    return true;
      }

      void setWorld(String parseWorld)
      {
	    parseWorld = parseWorld.replaceAll("\\s", "");
	    for (int a = 0; a < worldWidth * worldHigth; a++)
	    {
		  char charAt = parseWorld.charAt(a);
		  world[a % worldHigth][worldHigth - 1 - (a / worldHigth)] = parseWorld.charAt(a);
		  if (a == worldWidth * worldHigth - 1)
		  {
			a = a + 20;
		  }
	    }
      }

      void clearTraces()
      {
	    predatorTraces = new ArrayList<>();
	    for (int a = 0; a < getNumberOfPreditors(); a++)
	    {
		  predatorTraces.add(new ArrayList<Point>());
		  for (int b = 0; b < historyLength; b++)
		  {
			predatorTraces.get(a).add(new Point(-1, -1));
		  }
	    }
	    preyTraces = new ArrayList<>();
	    for (int a = 0; a < getNumberOfPrey(); a++)
	    {
		  preyTraces.add(new ArrayList<Point>());
		  for (int b = 0; b < historyLength; b++)
		  {
			preyTraces.get(a).add(new Point(-1, -1));
		  }
	    }
      }

      /**
       * *
       *
       * @param parsePoint
       * @param prey which one from 0 to numPrey-1
       * @param time from 0 to histLength-1
       */
      void addPreyTraceStep(Point location, int prey, int time)
      {
	    preyTraces.get(prey).get(time).x = location.x;
	    preyTraces.get(prey).get(time).y = location.y;
      }

      /**
       * *
       *
       * @param parsePoint
       * @param prey which one from 0 to numPrey-1
       * @param time from 0 to histLength-1
       */
      void addPredatorTraceStep(Point location, int predator, int time)
      {
	    predatorTraces.get(predator).get(time).x = location.x;
	    predatorTraces.get(predator).get(time).y = location.y;
      }

      void setHistoryLength(int input)
      {
	    historyLength = input;
      }

      private static Integer MapToRange(int toMap, int inRangeMin, int inRangeMax, int outRangeMin, int outRangeMax)
      {
	    inRangeMax = inRangeMax - 1;
	    outRangeMax = outRangeMax - 1;
	    if (toMap < inRangeMin || toMap > inRangeMax)
	    {
		  System.err.println("toMap out of range");
		  System.exit(toMap);
	    }
	    if (toMap == inRangeMax)
	    {
		  return outRangeMax;
	    }
	    if (outRangeMin == inRangeMax)
	    {
		  return outRangeMax;
	    }
	    return Math.min(outRangeMax, Math.max(outRangeMin, Math.round((((float) (toMap - inRangeMin)) / (inRangeMax - inRangeMin)) * outRangeMax - outRangeMin) + outRangeMin));
      }

      /**
       * @return the numberOfPreditors
       */
      public int getNumberOfPreditors()
      {
	    return numberOfPreditors;
      }

      /**
       * @return the numberOfPrey
       */
      public int getNumberOfPrey()
      {
	    return numberOfPrey;
      }

      void setAutoPause(Point autoPauseIn)
      {
	    autoPause = autoPauseIn;
      }
}
